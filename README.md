# SatNOGS Live Data GUI

This is the gui version of the SatNOGS Live Data, which fetches data from the API, written in python.

### TODO:
- Make it auto refresh and recreate table
- Make filtering work?!
- Visualize data to a plot
- Add more data from the API
