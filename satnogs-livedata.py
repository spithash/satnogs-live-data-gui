#!/usr/bin/env python3

import configparser
import signal
import sys
import pandas as pd
from pandastable import Table
from tkinter import *


def signal_handler():
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)

# Process commandline options and parse configuration
cfg = configparser.ConfigParser(inline_comment_prefixes=('#', ';'))
cfg.read('configuration.ini')

SATNOGS_DB_API_KEY = cfg.get('Credentials', 'SATNOGS_DB_API_KEY')

url = 'https://db.satnogs.org/api/transmitters/?api_key={}&format=json'.format(SATNOGS_DB_API_KEY)

df = pd.read_json(url)

# All available fields/columns of the 'transmitters' API are:
# 'uuid' 'description' 'alive' 'type' 'uplink_low' 'uplink_high' 'uplink_drift'
# 'downlink_low' 'downlink_high' 'downlink_drift' 'mode' 'mode_id' 'uplink_mode' 'invert' 'baud'
# 'norad_cat_id' 'status' 'updated' 'citation' 'service' 'coordination' 'coordination_url'
selection = ['type', 'uuid', 'description', 'status', 'alive', 'updated']
df = df[selection]


# We select a maximum of 3000 entries
data = df.head(3000)


# Create the model


class SatNOGSLiveDataModel(Frame):

    def __init__(self, parent=None):
        self.parent = parent
        Frame.__init__(self)
        self.main = self.master
        self.main.geometry('1405x900+200+100')
        self.main.title('SatNOGS Live Data')
        f = Frame(self.main)
        f.pack(fill=BOTH, expand=1)
        self.table = pt = Table(f, dataframe=data,
                                showtoolbar=False, showstatusbar=True)
        pt.columncolors['uuid'] = '#7389CA'
        pt.columncolors['description'] = '#7389CA'
        pt.columncolors['updated'] = '#7389CA'
        pt.columncolors['type'] = '#7389CA'
        mask_1 = pt.model.df['status'] == 'inactive'
        mask_2 = pt.model.df['status'] == 'invalid'
        mask_3 = pt.model.df['status'] == 'active'
        mask_4 = pt.model.df['type'] == 'Transceiver'
        mask_5 = pt.model.df['type'] == 'Transmitter'
        mask_6 = pt.model.df['type'] == 'Transponder'
        mask_7 = pt.model.df['alive'] == True
        mask_8 = pt.model.df['alive'] == False
        pt.setColorByMask('status', mask_1, '#ff9837')
        pt.setColorByMask('status', mask_2, '#ff6137')
        pt.setColorByMask('status', mask_3, '#58ff37')
        pt.setColorByMask('type', mask_4, '#B4FF96')
        pt.setColorByMask('type', mask_5, '#A5FFE0')
        pt.setColorByMask('type', mask_6, '#F8D58F')
        pt.setColorByMask('alive', mask_7, '#58ff37')
        pt.setColorByMask('alive', mask_8, '#ff5837')
        pt.sortTable(columnIndex=5, ascending=0, index=False)
        pt.redraw()
        pt.show()
        return


app = SatNOGSLiveDataModel()
app.mainloop()
